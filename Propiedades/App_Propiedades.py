import pandas as pd
import numpy as np
#from datetime import datetime
#from fbprophet import Prophet
#import seaborn as sns
import streamlit as st
import os
import base64
import pickle
from scipy.special import boxcox, inv_boxcox
from catboost import CatBoostRegressor

#En vista a tener que usar ```Prophet```, voy a reservar el listado de festivos de Chile para 2020.


#Constantes obtenidas del análisis y modelamiento para la transformación de Box Cox

LAMBDA_SUR_ARG=-0.30646240142568787
LAMBDA_PRICE_ARG=-0.11103544047717187

LAMBDA_SUR_COL=-0.2269856136783882
LAMBDA_PRICE_COL=-0.08474175842364569



st.title('🚧 Demo Predicción de Precios USD 🚧')

st.sidebar.markdown("## 1)👈Elige la ubicacion específica")

# Selección va en la barra lateral

@st.cache
def carga_data():
    df=pd.read_csv('ciudades.csv')
    return df
df=carga_data()



opcion_pais=st.sidebar.selectbox(
    '¿Qué ubicación te gustaría elegir? ',
    df.pais.unique())

opcion_provincia=st.sidebar.selectbox(
    'Indica la provincia',
    df.loc[df.pais==opcion_pais,'provincia_departamento'].unique())


opcion_ciudad=st.sidebar.selectbox(
    'Indica la ciudad',
    df.loc[df.provincia_departamento==opcion_provincia,'ciudad'].unique())



habitaciones=st.slider('Número de habitaciones',min_value=3,max_value=12,step=1,value=5)
dormitorios=st.slider('Número de dormitorios',min_value=1,max_value=habitaciones-2,step=1)
bannos=st.slider('Número de baños',min_value=1,max_value=habitaciones-dormitorios,step=1)
superficie=st.number_input('Superficie Total',min_value=30,max_value=350,step=10)




model=CatBoostRegressor()
if opcion_pais=='Argentina':
    model.load_model("cb_arg")
    superficie=boxcox(superficie,LAMBDA_SUR_ARG)
    lambda_price=LAMBDA_PRICE_ARG
    
else:
    model.load_model("cb_col")
    superficie=boxcox(superficie,LAMBDA_SUR_COL)
    lambda_price=LAMBDA_PRICE_COL


prediccion=model.predict([opcion_provincia,opcion_ciudad,habitaciones,dormitorios,bannos,superficie])
prediccion=inv_boxcox(prediccion,lambda_price)

texto='El valor predicho es USD '+str(format(int(round(prediccion))))
st.title(texto)

st.write("Pendiente: incorporar intervalos de confianza en las estimaciones")